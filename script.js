var canvas = document.getElementById("canvas");

canvas.width = window.innerWidth;
canvas.height = window.innerHeight;

// Initialize the GL context
var gl = canvas.getContext("webgl");
if (!gl) {
  console.error("Unable to initialize WebGL.");
}

//Time
var time = 0.0;

//************** Shader sources **************

//vertex shader代码
var vertexSource = `
//设置顶点位置
attribute vec2 position;
void main() {
  //将向量（position，0.0,1.0）赋值给预定义的变量gl_Position，以生成当前顶点的裁剪空间坐标
  gl_Position = vec4(position, 0.0, 1.0);
}
`;

//fragment shader代码
var fragmentSource = `
// 设置float类型精度
precision highp float;

//分别为长和宽
uniform float width;
uniform float height;

//分辨率=（宽，高）
vec2 resolution = vec2(width, height);

//时间
uniform float time;

//贝塞尔曲线中点数
#define POINT_COUNT 15

//点的颜色
vec2 points[POINT_COUNT];

//速度,长度,强度,半径
const float speed = -0.1;
const float len = 0.25;
float intensity = 1.4;
float radius = 0.017;

//https://www.shadertoy.com/view/MlKcDD
//Signed distance to a quadratic bezier
//计算点到贝塞尔曲线的距离
float sdBezier(vec2 pos, vec2 A, vec2 B, vec2 C){    
  vec2 a = B - A;
  vec2 b = A - 2.0*B + C;
  vec2 c = a * 2.0;
  vec2 d = A - pos;

  float kk = 1.0 / dot(b,b);
  float kx = kk * dot(a,b);
  float ky = kk * (2.0*dot(a,a)+dot(d,b)) / 3.0;
  float kz = kk * dot(d,a);      

  float res = 0.0;

  float p = ky - kx*kx;
  float p3 = p*p*p;
  float q = kx*(2.0*kx*kx - 3.0*ky) + kz;
  float h = q*q + 4.0*p3;

  if(h >= 0.0){ 
    h = sqrt(h);
    vec2 x = (vec2(h, -h) - q) / 2.0;
    vec2 uv = sign(x)*pow(abs(x), vec2(1.0/3.0));
    float t = uv.x + uv.y - kx;
    t = clamp( t, 0.0, 1.0 );

    // 1 root
    vec2 qos = d + (c + b*t)*t;
    res = length(qos);
  }else{
    float z = sqrt(-p);
    float v = acos( q/(p*z*2.0) ) / 3.0;
    float m = cos(v);
    float n = sin(v)*1.732050808;
    vec3 t = vec3(m + m, -n - m, n - m) * z - kx;
    t = clamp( t, 0.0, 1.0 );

    // 3 roots
    vec2 qos = d + (c + b*t.x)*t.x;
    float dis = dot(qos,qos);
        
    res = dis;

    qos = d + (c + b*t.y)*t.y;
    dis = dot(qos,qos);
    res = min(res,dis);
    
    qos = d + (c + b*t.z)*t.z;
    dis = dot(qos,qos);
    res = min(res,dis);

    res = sqrt( res );
  }
    
  return res;
}


//http://mathworld.wolfram.com/HeartCurve.html
//心形曲线函数
vec2 getHeartPosition(float t){
  return vec2(16.0 * sin(t) * sin(t) * sin(t),
              -(13.0 * cos(t) - 5.0 * cos(2.0*t)
              - 2.0 * cos(3.0*t) - cos(4.0*t)));
}

//https://www.shadertoy.com/view/3s3GDn
//根据距离、半径和强度获取发光强度
float getGlow(float dist, float radius, float intensity){
  return pow(radius/dist, intensity);
}

//计算从一个点到下一个点之间的贝塞尔曲线段的距离
float getSegment(float t, vec2 pos, float offset, float scale){
  for(int i = 0; i < POINT_COUNT; i++){
    points[i] = getHeartPosition(offset + float(i)*len + fract(speed * t) * 6.28);
  }
    
  vec2 c = (points[0] + points[1]) / 2.0;
  vec2 c_prev;
  float dist = 10000.0;
    
  for(int i = 0; i < POINT_COUNT-1; i++){
    //https://tinyurl.com/y2htbwkm
    c_prev = c;
    c = (points[i] + points[i+1]) / 2.0;
    dist = min(dist, sdBezier(pos, scale * c_prev, scale * points[i], scale * c));
  }
  return max(0.0, dist);
}

void main(){
  //获取当前像素在画布上的位置
  vec2 uv = gl_FragCoord.xy/resolution.xy;
  //获取长和宽的比例
  float widthHeightRatio = resolution.x/resolution.y;
  vec2 centre = vec2(0.5, 0.5);
  //将当前像素的位置映射到[-1,1]范围内
  vec2 pos = centre - uv;
  //调整高度，将心形图案上移
  pos.y /= widthHeightRatio;
  //Shift upwards to centre heart
  // pos.y += 0.02;
  pos.y += 0.06;
  //设置缩放
  // float scale = 0.000015 * height;
  float scale = 0.0000075 * height;
  
  float t = time;
    
  //Get first segment
  //获取第一段贝塞尔曲线距离和发光强度
  //定义变量dist，调用getSegment函数，并传入t、pos、0.0和scale作为参数。
    float dist = getSegment(t, pos, 0.0, scale);

    //定义变量glow，调用getGlow函数，并传入dist、radius和intensity作为参数。
    float glow = getGlow(dist, radius, intensity);

    //定义变量col为一个vec3对象，里面的三个分量都是0.0。
    vec3 col = vec3(0.0);

    //将smoothstep函数输出值与10相乘，并加到col上去，产生白色的中心效果。
    col += 10.0 * vec3(smoothstep(0.003, 0.001, dist));

    //将glow值与1.0、0.05和0.3相乘，加到col上去，产生粉色的光晕效果。
    col += glow * vec3(1.0, 0.05, 0.3);
    // col += glow * vec3(1.0, 0.0, 0.0);

    //获取第二个区间
    dist = getSegment(t, pos, 3.14, scale);
    glow = getGlow(dist, radius, intensity);

    //再次将smoothstep函数输出值与10相乘，并加到col上去，产生白色的中心效果。
    col += 10.0 * vec3(smoothstep(0.003, 0.001, dist));

    //将glow值与0.1、0.4和1.0相乘，加到col上去，产生蓝色的光晕效果。
    col += glow * vec3(0.1, 0.4, 1.0);
    // col += glow * vec3(0.0, 0.0, 1.0);

    //应用色调映射，通过指数函数和-col产生输出。
    col = 1.0 - exp(-col);

    //应用伽马校正，通过指数值为0.4545的vec3对象来实现。
    col = pow(col, vec3(0.4545));

    //将结果输出到屏幕上，这里使用了vec4对象，其RGB分量为col，透明度为1.0。
    gl_FragColor = vec4(col, 1.0);

}
`;

//************** Utility functions **************

window.addEventListener("resize", onWindowResize, false);

function onWindowResize() {
  canvas.width = window.innerWidth;
  canvas.height = window.innerHeight;
  gl.viewport(0, 0, canvas.width, canvas.height);
  gl.uniform1f(widthHandle, window.innerWidth);
  gl.uniform1f(heightHandle, window.innerHeight);
}

//Compile shader and combine with source
function compileShader(shaderSource, shaderType) {
  var shader = gl.createShader(shaderType);
  gl.shaderSource(shader, shaderSource);
  gl.compileShader(shader);
  if (!gl.getShaderParameter(shader, gl.COMPILE_STATUS)) {
    throw "Shader compile failed with: " + gl.getShaderInfoLog(shader);
  }
  return shader;
}

//From https://codepen.io/jlfwong/pen/GqmroZ
//Utility to complain loudly if we fail to find the attribute/uniform
function getAttribLocation(program, name) {
  var attributeLocation = gl.getAttribLocation(program, name);
  if (attributeLocation === -1) {
    throw "Cannot find attribute " + name + ".";
  }
  return attributeLocation;
}

function getUniformLocation(program, name) {
  var attributeLocation = gl.getUniformLocation(program, name);
  if (attributeLocation === -1) {
    throw "Cannot find uniform " + name + ".";
  }
  return attributeLocation;
}

//************** Create shaders **************

//Create vertex and fragment shaders
var vertexShader = compileShader(vertexSource, gl.VERTEX_SHADER);
var fragmentShader = compileShader(fragmentSource, gl.FRAGMENT_SHADER);

//Create shader programs
var program = gl.createProgram();
gl.attachShader(program, vertexShader);
gl.attachShader(program, fragmentShader);
gl.linkProgram(program);

gl.useProgram(program);

//Set up rectangle covering entire canvas
//定义一个包含整个画布的矩形，左上角为(-1, 1)，右下角为(1, -1)
var vertexData = new Float32Array([
  -1.0,
  1.0, // top left
  -1.0,
  -1.0, // bottom left
  1.0,
  1.0, // top right
  1.0,
  -1.0, // bottom right
]);

//Create vertex buffer
//创建顶点缓冲区对象
var vertexDataBuffer = gl.createBuffer();

//绑定顶点缓冲区，将数组数据写入缓冲区
gl.bindBuffer(gl.ARRAY_BUFFER, vertexDataBuffer);
gl.bufferData(gl.ARRAY_BUFFER, vertexData, gl.STATIC_DRAW);

// Layout of our data in the vertex buffer
//指定定点属性
var positionHandle = getAttribLocation(program, "position");

//激活顶点属性
gl.enableVertexAttribArray(positionHandle);

//设置顶点属性指针
gl.vertexAttribPointer(
  positionHandle,
  2, // position is a vec2 (2 values per component)  // 每个顶点包含两个元素
  gl.FLOAT, // each component is a float  // 元素类型为浮点型
  false, // don't normalize values     // 是否对数据进行归一化处理
  2 * 4, // two 4 byte float components per vertex (32 bit float is 4 bytes)   // 每个顶点占8个字节（32位浮点数是4个字节）
  0 // how many bytes inside the buffer to start from   // 每个顶点从缓冲区起始位置开始读取数据
);

//Set uniform handle
//设置uniform变量
var timeHandle = getUniformLocation(program, "time");
var widthHandle = getUniformLocation(program, "width");
var heightHandle = getUniformLocation(program, "height");

//设置视口的宽和高
gl.uniform1f(widthHandle, window.innerWidth);
gl.uniform1f(heightHandle, window.innerHeight);

var lastFrame = Date.now();
var thisFrame;

function draw() {
  //Update time
  //更新时间
  thisFrame = Date.now();
  time += (thisFrame - lastFrame) / 1000;
  lastFrame = thisFrame;

  //Send uniforms to program
  //将uniform变量传递给着色器程序
  gl.uniform1f(timeHandle, time);
  //Draw a triangle strip connecting vertices 0-4
  //绘制三角形带，连接顶点0-4
  gl.drawArrays(gl.TRIANGLE_STRIP, 0, 4);

  //请求浏览器调用函数draw来进行下一次绘制
  requestAnimationFrame(draw);
}

//开始绘制
draw();
